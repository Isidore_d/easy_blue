// -*- mode: c++; coding: utf-8; tab-width: 4 -*-

// Copyright (C) 2014, Oscar Acena <oscaracena@gmail.com>
// This software is under the terms of Apache License v2 or later.

#ifndef _GATTSERVICES_H_
#define _GATTSERVICES_H_

#include <boost/python/dict.hpp>
#include <map>

#define EIR_NAME_SHORT     0x08  /* shortened local name */
#define EIR_NAME_COMPLETE  0x09  /* complete local name */

#define BLE_EVENT_TYPE     0x05
#define BLE_SCAN_RESPONSE  0x04

#include <boost/python.hpp>

class DiscoveryService {
public:
	DiscoveryService(const std::string device="hci0");
	virtual ~DiscoveryService();
	boost::python::dict discover(int timeout, bool detect_inactiv=false);

	virtual void device_discovered(std::string addr, std::string name);
protected:
	void enable_scan_mode();
	void get_advertisements(int timeout, boost::python::dict & ret, bool detect_inactiv=false);
	virtual void process_input(unsigned char* buffer, int size,
			boost::python::dict & ret, bool detect_inactiv);
	std::string parse_name(uint8_t* data, size_t size);
	void disable_scan_mode();

	std::string _device;
	int _device_desc;
	int _timeout;
};

using namespace boost::python;

struct BaseDiscoveryService : DiscoveryService, wrapper<DiscoveryService>
{
	BaseDiscoveryService(const std::string device="hci0") :
		DiscoveryService(device)
		{}
	virtual ~BaseDiscoveryService() {}


	void device_discovered(std::string addr, std::string name)
    {
        if (override device_discovered = this->get_override("device_discovered"))
            device_discovered(addr, name); // *note*
        DiscoveryService::device_discovered(addr, name);
    }
    void default_device_discovered(std::string addr, std::string name)
	{ 
		this->DiscoveryService::device_discovered(addr, name); 
	}
};

BOOST_PYTHON_MODULE(easy_blue)
{
  class_<BaseDiscoveryService, boost::noncopyable>("DiscoveryService")
    .def("device_discovered", &DiscoveryService::device_discovered, &BaseDiscoveryService::default_device_discovered)
	
	.def(init<>())
	.def(init<std::string>())
    .def("discover", &DiscoveryService::discover)
	
    ;
/*
  class_<DiscoveryService>("DiscoveryService", init<>())
		.def(init<std::string>())
        .def("discover", &DiscoveryService::discover)
    ;
*/
}

#endif // _GATTSERVICES_H_
