
PYTHON_VERSION = 3.4
PYTHON_INCLUDE = /usr/include/python$(PYTHON_VERSION)
 
BOOST_INC = /usr/include
BOOST_LIB = /usr/lib

TARGET = easy_blue

CC = g++

INCLUDE = -I/usr/include/python$(PYTHON_VERSION) -I$(PYTHON_INCLUDE) -Iinclude/

LIBS = -L/usr/local/lib -L/usr/lib/python3/dist-packages/ -L/usr/lib/python$(PYTHON_VERSION)/config -L$(BOOST_LIB) -L/usr/lib/python$(PYTHON_VERSION)/config -L/usr/lib/python3.4/config-3.4m-x86_64-linux-gnu/

LLIBS = -lboost_python-py34  -lpython$(PYTHON_VERSION) -lbluetooth -lboost_thread

SPE = $$(pkg-config --libs glib-2.0) -ggdb `pkg-config --cflags --libs glib-2.0`

BFLAGS = -shared -Wl,--export-dynamic -Wl,-soname,"easy_blue.so" -Wall -fPIC

RPATH = -Wl,-rpath /usr/local/lib/python3.6/site-packages/ -Wl,-rpath /usr/lib/x86_64-linux-gnu/

FLAGS = $(BFLAGS) $(RPATH) $(INCLUDE) $(LIBS) $(SPE)

vpath %.c bluez/attrib
vpath %.c bluez/src
vpath %.c bluez/src/shared
vpath %.c bluez/lib
vpath %.c bluez/btio

SRC = $(wildcard src/*.cpp)

OBJ= $(SRC:.c=.o)

all: $(TARGET).so

$(TARGET).so: $(OBJ)
	$(CC) $(FLAGS) $(OBJ) -o $@ $(LLIBS)

%.o: %.c
	$(CC) -fPIC -o $@


	

.PHONY: clean

clean:
	rm -f *.o *.so* *~



