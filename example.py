### Import
import easy_blue

### Héritage
class MonDiscoverer(easy_blue.DiscoveryService):
	def __init__(self):
		easy_blue.DiscoveryService.__init__(self, "hci0")

	### fonction scanne
	def scanne(self):
		### Do some stuff
		pass
	
	### overide ceci
	def device_discovered(self, addr, name):
		### print
		print("ahahaah", addr, name)

		### do something else
		

### Avec 1 class hérité
obj = MonDiscoverer()
obj.discover(5, True)

### Avec class de base ( obtient 1 dict )
base = easy_blue.DiscoveryService()
mon_dict = base.discover(5, True)



